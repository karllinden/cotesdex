# Changelog
All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Changed
 - The program does not create trailing spaces in the outputs anymore.
 - Input files no longer need to have lines with trailing spaces to
   separate paragraphs.
 - The program now requires GOP-4.
 - The remove command has been dropped. Output files are now removed
   unconditionally.
 - The tests have been redesigned to source a helper script and reusing
   functions from it. This reduces the space occupied by the generated
   tests. Also the diagnostics have been vastly improved, so that the
   poor tester does not have to dig in the cotesdex temporary
   directories.
 - Bugfixes:
   - A segfault has been fixed.
 - The project is now maintained at
   [gitlab](https://gitlab.com/karllinden/cotesdex).
 - The project uses waf as build system.
 - The old ChangeLog has been pruned in favor this file.

[Unreleased]: https://gitlab.com/karllinden/cotesdex/compare/v0.3.0...HEAD
