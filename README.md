# COTESDEX

Author: Karl Linden

Report bugs to: https://gitlab.com/karllinden/cotesdex/issues

Homepage: https://gitlab.com/karllinden/cotesdex


## Description

Cotesdex is short for Combined Test, Documentation and Example. It is a
small utility to create a test, a piece of documentation and an example
from one common input file that has extra cotesdex tags in its comments.
It is fast, reasonably lightweight and completely written in C.

Currently it can parse C source files and shell scripts.

From the input files it can generate shell scripts used for testing,
asciidoc documentation and stripped plain examples (without cotesdex
tags).


## Dependencies

To run the program the [GOP library](https://gitlab.com/karllinden/gop)
is needed. This library is also required during build time.

The tests are run with [GNU Bash](https://www.gnu.org/software/bash/),
with the help of
[Coreutils](https://www.gnu.org/software/coreutils/coreutils.html)
and [Diffutils](https://www.gnu.org/software/diffutils/).

The tests can be run in [valgrind](http://valgrind.org/), in which case
that program is required at runtime, although not at build time.

## Installation Instructions

You can build and install the program simply by running the below
commands in the source directory.
```shell
./waf configure
./waf
./waf install
```

Additionally, if you are building from Git you need to run
```shell
git submodule update --init
```
prior to the above commands.

For build options, refer to:
```shell
./waf --help
```

## Testing

Cotesdex comes with a (small) test suite. Before using the program it is
recommended to run the test suite using:
```shell
./waf check
```
