/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* asciidoc.c - routines needed to generate an asciidoc file */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "asciidoc.h"
#include "buffer-foreach-line.h"
#include "command.h"
#include "file-type.h"
#include "main.h"
#include "output.h"

#define oprint(...) output_print(cotesdex, __VA_ARGS__)
#define oputs(string) output_puts(cotesdex, (string))

struct print_line_s {
    const cotesdex_t * cotesdex;
    bool had_line_before;
};

static void __attribute__((nonnull))
print_header(const cotesdex_t * cotesdex,
             char header_char,
             const char * header)
{
    size_t n = strlen(header);
    output_puts(cotesdex, header);
    output_putchar(cotesdex, '\n');
    while (n-- > 0) {
        output_putchar(cotesdex, header_char);
    }
    output_putchar(cotesdex, '\n');
    return;
}

static void __attribute__((nonnull))
print_command_line(const char * line, void * argument)
{
    struct print_line_s * pl = (struct print_line_s *)argument;
    if (pl->had_line_before) {
        output_print(pl->cotesdex, " \\\n\t%s", line);
    } else {
        output_print(pl->cotesdex, " %s", line);
        pl->had_line_before = true;
    }
    return;
}

static int __attribute__((nonnull))
print_output_files(const cotesdex_t * cotesdex,
                   const command_t * command)
{
    for (const command_t * child = command_get_child(command);
          child != NULL;
          child = command_get_sister(child))
    {
        if (command_get_type(child) == COMMAND_OUTPUT_FILE) {
            char * file_name = command_get_argument(child);
            char * content = command_file_harvest_content(child);
            if (content == NULL) {
                return 1;
            }

            oprint("$ cat %s\n", file_name);
            if (*content != '\0') {
                oprint("%s\n", content);
            }

            free(content);
        }
    }
    return 0;
}

/* Returns 0 on success or 1 on failure. */
static int __attribute__((nonnull))
print_case(const cotesdex_t * cotesdex,
           const char * executable,
           const command_t * command,
           bool * has_printed_header)
{
    int return_value = 0;

    if (!*has_printed_header) {
        print_header(cotesdex, '-', "Some sample cases");
        oprint("These are some sample inputs and outputs that work\n"
               "with the example (the example program is called\n"
               "+%s+ in the shell extracts).\n", executable);
        *has_printed_header = true;
    }

    /*
     * Set if the current test command has an output file command. This
     * is only here as an optimization so that the child commands are
     * not reiterated if there are no output file commands.
     */
    bool has_output_file = false;

    /* Assume INT_MIN is not a valid status. */
    int status = INT_MIN;

    /* These must be free()'d. */
    char * command_line = NULL;
    char * stderr_content = NULL;
    char * stdin_content = NULL;
    char * stdout_content = NULL;

    command_t * child = command_get_child(command);
    while (child != NULL) {
        switch (command_get_type(child)) {
            case COMMAND_CMDLINE:
                command_line = command_harvest_content(child);
                child = command_unlink(child);
                break;
            case COMMAND_STATUS:
                status = atoi(command_get_argument(child));
                child = command_unlink(child);
                break;
            case COMMAND_STDERR:
                stderr_content = command_file_harvest_content(child);
                if (stderr_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;
            case COMMAND_STDIN:
                stdin_content = command_file_harvest_content(child);
                if (stdin_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;
            case COMMAND_STDOUT:
                stdout_content = command_file_harvest_content(child);
                if (stdout_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;

            /*
             * Cases that do not remove the command. The command is to
             * be used later on.
             */
            case COMMAND_OUTPUT_FILE:
                if (!has_output_file) has_output_file = true;
            default:
                child = command_get_sister(child);
                break;
        }
    }

    oputs("\n----\n");
    if (stdin_content != NULL) {
        oputs("$ cat input\n");
        if (stdin_content[0] != '\0') {
            oprint("%s\n", stdin_content);
        }
    }

    oprint("$ %s", executable);

    /* Print the command line arguments. */
    if (command_line != NULL) {
        struct print_line_s print_line_struct;
        print_line_struct.cotesdex = cotesdex;
        print_line_struct.had_line_before = false;
        buffer_foreach_line(command_line, &print_command_line,
                            &print_line_struct);
    }

    if (stdin_content != NULL) {
        oputs(" < input");
    }

    /* Make sure the output to stderr and stdout are correct. */
    if (stderr_content == NULL && stdout_content == NULL) {
        oputs(" &> /dev/null\n");
    } else /* (stderr_content != NULL || stdout_content != NULL) */ {
        if (stdout_content == NULL) {
            /* stdout_content == NULL => stderr_content != NULL */
            oputs(" 1> /dev/null\n");
            if (stderr_content[0] != '\0') {
                oprint("%s\n", stderr_content);
            }
        } else /* (stdout_content != NULL) */ {
            if (stderr_content != NULL) {
                oputs(" 2> stderr\n");
            } else {
                oputs(" 2> /dev/null\n");
            }
            if (stdout_content[0] != '\0') {
                oprint("%s\n", stdout_content);
            }
        }
    }

    if (status != INT_MIN) {
        oputs("$ echo $?\n");
        oprint("%d\n", status);
    }

    if (stderr_content != NULL && stdout_content != NULL) {
        oputs("$ cat stderr\n");
        if (stderr_content[0] != '\0') {
            oprint("%s\n", stderr_content);
        }
    }

    if (has_output_file) {
        if (print_output_files(cotesdex, command)) {
            goto error;
        }
    }

    oputs("$\n"
          "----\n");

    if (false) {
    error:
        return_value = 1;
    }

    free(command_line);
    free(stderr_content);
    free(stdin_content);
    free(stdout_content);
    return return_value;
}

int __attribute__((nonnull))
asciidoc(cotesdex_t * cotesdex)
{
    bool has_printed_header = false;

    output_set_file_type(cotesdex, FILE_TYPE_ASCIIDOC);

    char * title;
    char * description;
    char * license;
    get_meta(cotesdex, &title, &description, &license);

    /* Print the license if any. */
    if (license != NULL) {
        output_begin_comment(cotesdex);
        output_print_buffer(cotesdex, license);
        output_end_comment(cotesdex);
        output_putchar(cotesdex, '\n');
        free(license);
    }

    if (title != NULL) {
        print_header(cotesdex, '=', title);
        free(title);
    } else if (cotesdex->input_file_name != NULL) {
        print_header(cotesdex, '=', cotesdex->input_file_name);
    } else {
        /* This is a bad title. */
        print_header(cotesdex, '=', "Document from standard input");
    }

    oprint("Generated with %s.\n\n", PACKAGE_STRING);

    /* Print the description if any. */
    if (description != NULL) {
        print_header(cotesdex, '-', "Description");
        output_print_buffer(cotesdex, description);
        output_putchar(cotesdex, '\n');
        free(description);
    }

    if (cotesdex->input_file_type == FILE_TYPE_C) {
        print_header(cotesdex, '-', "Code");
        oprint("[source,C]\n"
               "----\n"
               "%s\n"
               "----\n\n", cotesdex->code);
    }

    const char * executable;
    if (cotesdex->executable_file_name != NULL) {
        const char * c = strrchr(cotesdex->executable_file_name, '/');

        /* The file name is an absolute path, there must be a slash. */
        assert(c != NULL);

        executable = c + 1;
    } else {
        executable = "example";
    }

    const command_t * root = cotesdex->commands;
    const command_t * command = command_get_child(root);
    while (command != NULL) {
        if (command_get_type(command) == COMMAND_TEST) {
            if (print_case(cotesdex, executable, command,
                            &has_printed_header))
            {
                return 1;
            }
        }
        command = command_get_sister(command);
    }

    return 0;
}
