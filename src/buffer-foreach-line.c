/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* buffer-foreach-line.c - the source file for buffer_foreach_line */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "buffer-foreach-line.h"
#include "xalloc.h"

/*
 * Buffer variables for buffer_foreach_line().
 * As memory optimization this buffer is saved between calls to
 * buffer_foreach_line() so that the buffer does not have to be
 * (re)allocated as much as it otherwise would have been. The memory
 * is free()'d upon exit.
 */
static bool buffer_line_atexit = false;
static char * buffer_line = NULL;
static size_t buffer_line_size = 0;

static void
buffer_line_free(void)
{
    free(buffer_line);
    return;
}

void __attribute__((nonnull (1,2)))
buffer_foreach_line(const char * restrict buffer,
                    void (*func)(const char *, void *),
                    void * argument)
{
    /* Make sure the buffer is free()'d upon exit. */
    if (!buffer_line_atexit) {
        if (atexit(&buffer_line_free)) {
            print_error(_("could not set atexit function"));
        }
        buffer_line_atexit = true;
    }

    /* The beginning of the line. */
    const char * begin = buffer;

    const char * end;
    while ((end = strchr(begin, '\n')) != NULL) {
        size_t len = (size_t)(end - begin);

        /* Make sure the buffer has sufficient size. */
        if (len >= buffer_line_size) {
            buffer_line_size = len + 1;
            free(buffer_line);
            buffer_line = xmalloc(buffer_line_size);
        }

        /* Fill in the line. */
        memcpy(buffer_line, begin, len);
        buffer_line[len] = '\0';

        /* Call the function. */
        (*func)(buffer_line, argument);

        /* Step to the next line. */
        begin = end + 1;
    }

    /*
     * This is the last line. The function can be called with this line
     * directly because the line is already NUL-terminated.
     */
    (*func)(begin, argument);
    return;
}
