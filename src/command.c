/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* command.c - functions to manage commands, header file */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "command.h"
#include "read-file.h"
#include "xalloc.h"

/* This is used to end the command tables. */
#define COMMAND_SYNOPSIS_INVALID \
    {COMMAND_INVALID, "invalid", false, false, NULL}

/* hide and unhide should be available at all indentation levels. */
#define COMMAND_SYNOPSIS_HIDE_UNHIDE \
    {COMMAND_HIDE,   "hide",   false, false, NULL}, \
    {COMMAND_UNHIDE, "unhide", false, false, NULL}

/*
 * The possible child commands for the output-file, stdin and stdout
 * commands.
 */
static const command_synopsis_t file_commands[] = {
    {COMMAND_CONTENT, "content", true,  false, NULL},
    {COMMAND_FILE,    "file",    false, true,  NULL},
    COMMAND_SYNOPSIS_HIDE_UNHIDE,
    COMMAND_SYNOPSIS_INVALID
};

/* The possible child commands for the test command. */
static const command_synopsis_t test_commands[] = {
    {COMMAND_CMDLINE,     "cmdline",     true,  false, NULL},
    {COMMAND_OUTPUT_FILE, "output-file", false, true,  file_commands},
    {COMMAND_STATUS,      "status",      false, true,  NULL},
    {COMMAND_STDERR,      "stderr",      false, false, file_commands},
    {COMMAND_STDIN,       "stdin",       false, false, file_commands},
    {COMMAND_STDOUT,      "stdout",      false, false, file_commands},
    COMMAND_SYNOPSIS_HIDE_UNHIDE,
    COMMAND_SYNOPSIS_INVALID
};

/* All available commands directly below the root. */
static const command_synopsis_t root_commands[] = {
    {COMMAND_DESCRIPTION, "description", true,  false, NULL},
    {COMMAND_HIDE,        "hide",        false, false, NULL},
    {COMMAND_LICENSE,     "license",     true,  false, NULL},
    {COMMAND_TEST,        "test",        false, false, test_commands},
    {COMMAND_TITLE,       "title",       false, true,  NULL},
    {COMMAND_UNHIDE,      "unhide",      false, false, NULL},
    COMMAND_SYNOPSIS_HIDE_UNHIDE,
    COMMAND_SYNOPSIS_INVALID
};

/* The root synopsis used to generate the root command. */
static command_synopsis_t root_synopsis =
    {COMMAND_ROOT, "root", false, false, root_commands};

command_t * __attribute__((nonnull, returns_nonnull))
command_new(const command_synopsis_t * synopsis)
{
    command_t * command = xmalloc(sizeof(command_t));

    command->synopsis = synopsis;
    command->argument = NULL;
    command->content = NULL;
    command->content_len = 0;
    command->column = 0;
    command->parent = NULL;
    command->child = NULL;
    command->brother = NULL;
    command->sister = NULL;

    return command;
}

command_t * __attribute__((returns_nonnull))
command_root(void)
{
    return command_new(&root_synopsis);
}

void __attribute__((nonnull))
command_destroy(command_t * command)
{
    free(command->argument);
    free(command->content);
    free(command);

    return;
}

void __attribute__((nonnull))
command_destroy_recursive (command_t * command)
{
    command_t * child = command_get_child(command);
    while (child != NULL) {
        command_t * sister = command_get_sister(child);
        command_destroy_recursive(child);
        child = sister;
    }

    command_destroy(command);
    return;
}

command_t * __attribute__((nonnull))
command_unlink(command_t * command)
{
    command_t * brother = command_get_brother(command);
    command_t * sister = command_get_sister(command);

    if (brother != NULL) {
        brother->sister = sister;
    } else {
        /*
         * The parent is guaranteed to be non-NULL because the command
         * is not allowed to be the root element.
         */
        command_t * parent = command_get_parent(command);
        parent->child = sister;
    }

    if (sister != NULL) {
        sister->brother = brother;
    }

    command_destroy_recursive(command);
    return sister;
}


void __attribute__((nonnull))
command_add_sister(command_t * command, command_t * sister)
{
    while (command->sister != NULL) command = command->sister;
    sister->brother = command;
    command->sister = sister;
    return;
}

void __attribute__((nonnull))
command_add_child(command_t * parent, command_t * child)
{
    if (parent->child != NULL) {
        command_add_sister(parent->child, child);
    } else {
        parent->child = child;
    }
    child->parent = parent;
    return;
}

char * __attribute__((nonnull))
command_harvest_argument(command_t * command)
{
    char * argument = command->argument;
    command->argument = NULL;
    return argument;
}

char * __attribute__((nonnull))
command_harvest_content(command_t * command)
{
    char * content = command->content;
    command->content = NULL;
    return content;
}

char * __attribute__((nonnull))
command_file_harvest_content(const command_t * command)
{
    /* A nifty assertion... */
    assert(command_get_type(command) == COMMAND_OUTPUT_FILE ||
           command_get_type(command) == COMMAND_STDERR ||
           command_get_type(command) == COMMAND_STDIN ||
           command_get_type(command) == COMMAND_STDOUT);

    for (command_t * child = command_get_child(command);
          child != NULL;
          child = command_get_sister(child))
    {
        if (command_get_type(child) == COMMAND_CONTENT) {
            char * content = command_harvest_content(child);
            assert(content != NULL);
            return content;
        } else if (command_get_type(child) == COMMAND_FILE) {
            /* Read the content of the file. */
            const char * file_name = command_get_argument(child);
            size_t buffer_len;
            return read_file(file_name, &buffer_len);
        }
    }

    /* The file should be empty. */
    char * content = xmalloc(1);
    content[0] = '\0';
    return content;
}
