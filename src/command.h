/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* command.h - functions to manage commands, header file */

#ifndef COMMAND_H
# define COMMAND_H

# include <stdbool.h>
# include <stddef.h>

enum command_type_e {
    /* Alphabetically ordered. */
    COMMAND_CMDLINE,
    COMMAND_CONTENT,
    COMMAND_DESCRIPTION,
    COMMAND_FILE,
    COMMAND_HIDE,
    COMMAND_INVALID,
    COMMAND_LICENSE,
    COMMAND_OUTPUT_FILE,
    COMMAND_ROOT,
    COMMAND_STATUS,
    COMMAND_STDERR,
    COMMAND_STDOUT,
    COMMAND_STDIN,
    COMMAND_TEST,
    COMMAND_TITLE,
    COMMAND_UNHIDE
};
typedef enum command_type_e command_type_t;

struct command_synopsis_s {
    command_type_t type;
    const char * name;
    bool can_have_content;
    bool needs_argument;

    /* A list of possible children. */
    const struct command_synopsis_s * children;
};
typedef struct command_synopsis_s command_synopsis_t;

struct command_s {
    const command_synopsis_t * synopsis;
    char * argument;
    char * content;
    size_t content_len;

    /*
     * This member shows at which column the tag of this command was
     * found.
     */
    unsigned short column;

    struct command_s * parent;
    struct command_s * child;
    struct command_s * brother;
    struct command_s * sister;
};
typedef struct command_s command_t;

#define command_get_type(command) ((command)->synopsis->type)
#define command_get_argument(command) ((command)->argument)
#define command_get_content(command) ((command)->content)
#define command_get_child(command) ((command)->child)
#define command_get_brother(command) ((command)->brother)
#define command_get_sister(command) ((command)->sister)
#define command_get_parent(command) ((command)->parent)

/*
 * Creates a new command.
 * Returns a pointer to the newly allocated command.
 */
command_t * command_new(const command_synopsis_t * synopsis)
    __attribute__((nonnull, returns_nonnull));

/*
 * Creates a root command.
 * Returns a pointer to the newly allocated command.
 */
command_t * command_root(void) __attribute__((returns_nonnull));

/* Destroys the command without altering the links. */
void command_destroy(command_t * command)
    __attribute__((nonnull));

/* Destroys the command and all of its children recursively. */
void command_destroy_recursive(command_t * command)
    __attribute__((nonnull));

/*
 * Unlinks the command from the tree and destroys it. The command is not
 * allowed to be the root element of the command tree.
 * The function returns the sister of the destroyed command.
 */
command_t * command_unlink(command_t * commmand)
    __attribute__((nonnull));

/* Adds a sister to the command. */
void command_add_sister(command_t * command, command_t * sister)
    __attribute__((nonnull));

/* Adds a child command to the command. */
void command_add_child(command_t * parent, command_t * child)
    __attribute__((nonnull));

/* Removes (detaches) the argument from the command and returns it. */
char * command_harvest_argument(command_t * command)
    __attribute__((nonnull));

/* Removes (detaches) the content from the command and returns it. */
char * command_harvest_content(command_t * command)
    __attribute__((nonnull));

/*
 * Call this with a file command (that is stderr, stdin, stdout or
 * output-file) to get the content of the file. The file end will be
 * shown by the NUL character and the UNIX file end (newline) will be
 * removed so that there is no extra line break at the end of the file.
 *
 * The result should be free()'d after use.
 *
 * Returns the content of the file or NULL on error.
 */
char * command_file_harvest_content(const command_t * command)
    __attribute__((nonnull));

#endif /* !COMMAND_H */
