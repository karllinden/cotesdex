/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* file_type.c - routines and data types for handling file types. */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "file-type.h"

/* All known markups. */
static markup_t markup_asciidoc = {NULL,  NULL, NULL,  NULL,  "//"};
static markup_t markup_c        = {"/*",  "*/", " */", " *",   "//"};
static markup_t markup_raw      = {NULL,  NULL, NULL,  NULL,  "#" };
static markup_t markup_shell    = {NULL,  NULL, NULL,  NULL,  "#" };

file_type_t __attribute__((pure))
file_type_autodetect(const char * file_name)
{
    if (file_name != NULL) {
        const char * last_dot = strrchr(file_name, '.');
        if (last_dot != NULL) {
            const char * suffix = last_dot + 1;
            if ((suffix[0] == 'c' || suffix[0] == 'h') &&
                 suffix[1] == '\0')
            {
                return FILE_TYPE_C;
            } else {
                return FILE_TYPE_RAW;
            }
        } else {
            /* There is no suffix. Probably it is a raw file... */
            return FILE_TYPE_RAW;
        }
    } else {
        /*
         * There is no way of guessing what the file type is so
         * let's assume it is C.
         */
        return FILE_TYPE_C;
    }
}

const markup_t * __attribute__((const))
file_type_get_markup(file_type_t file_type)
{
    switch (file_type) {
        case FILE_TYPE_ASCIIDOC:
            return &markup_asciidoc;
        case FILE_TYPE_C:
            return &markup_c;
        case FILE_TYPE_RAW:
            return &markup_raw;
        case FILE_TYPE_SHELL:
            return &markup_shell;
        default:
            return NULL;
    }
}
