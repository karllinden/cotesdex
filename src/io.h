/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* io.h - input/output macros */

#ifndef IO_H
# define IO_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdio.h>

/*
 * Since this program is not multi-threaded locks only slow down the
 * program.
 */
# if HAVE_FEOF_UNLOCKED
#  define feof_unlocked
# endif /* HAVE_FEOF_UNLOCKED */
# if HAVE_FERROR_UNLOCKED
#  define ferror ferror_unlocked
# endif /* HAVE_FERROR_UNLOCKED */
# if HAVE_FPUTC_UNLOCKED
#  define fputc fputc_unlocked
# endif /* HAVE_FPUTC_UNLOCKED */
# if HAVE_FPUTS_UNLOCKED
#  define fputs fputs_unlocked
# endif /* HAVE_FPUTS_UNLOCKED */
# if HAVE_FREAD_UNLOCKED
#  define fread fread_unlocked
# endif /* HAVE_FREAD_UNLOCKED */

#endif /* !IO_H */
