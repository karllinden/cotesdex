/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* main.h - a header containing commonly used macros */

#ifndef MAIN_H
# define MAIN_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdbool.h>
# include <stdio.h>

# include "command.h"
# include "file-type.h"

/*
 * This is the program structure that will be sent around to reduce the
 * number of arguments needed to all functions and reduce the need of
 * static variables.
 */
struct cotesdex_s {
    /*
     * This is the code buffer that will be filled in when the input
     * file is parsed.
     */
    char * code;
    size_t code_len;

    /* This is the tree of all parsed commands. */
    command_t * commands;

    /* Variables to use for the output. */
    FILE * output_file;
    bool output_comment;
    const markup_t * output_markup;

    /*
     * The user specified base path, that is the path where the program
     * will look for files.
     */
    char * base_path;

    /* The input file name. */
    char * input_file_name;
    file_type_t input_file_type;

    /*
     * This is the file name of the executable that will be used for
     * testing.
     */
    char * executable_file_name;

    /* The file type of the input file represented as a string. */
    char * file_type_string;

    /*
     * The here string that will be used in the generated tests. NULL
     * for default.
     */
    char * here_string;

    /* The name of the file to which the output will be written to. */
    char * output_file_name;

    /* Set to 1 if the tests should be run in valgrind's memcheck. */
    int memcheck;

    /* The valgrind program. */
    char * valgrind;
};
typedef struct cotesdex_s cotesdex_t;

/*
 * This function retrives the metadata from the cotesdex structure to
 * the strings pointed to by title, description and license. The
 * affected commands will be destroyed.
 *
 * The returned strings should be free()'d after use.
 *
 * If a field could not be found string that corresponds to the field
 * is NULL.
 */
void get_meta(const cotesdex_t * cotesdex,
              char ** title,
              char ** description,
              char ** license)
    __attribute__((nonnull));

#endif /* !MAIN_H */
