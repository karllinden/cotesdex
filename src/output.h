/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* output.h - common functions for handling output of data, header. */

#ifndef OUTPUT_H
# define OUTPUT_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdarg.h>
# include <stdio.h>

#include "file-type.h"
#include "main.h"

#define output_set_file_type(cotesdex, file_type) \
    ((cotesdex)->output_markup = file_type_get_markup(file_type))
#define output_putchar(cotesdex, c) (fputc(c, (cotesdex)->output_file))
#define output_puts(cotesdex, string) \
    (fputs((string), (cotesdex)->output_file))

/*
 * This will initialize the output_file member of the cotesdex structure
 * and open the output file if necessary. The output_file_name will also
 * be set to an absolute path to the file.
 * Returns 0 on success or 1 on failure.
 */
int output_open_file(cotesdex_t * cotesdex,
                     const char * file_name)
    __attribute__((nonnull));

void output_close_file(const cotesdex_t * cotesdex)
    __attribute__((nonnull));

void output_vprint(const cotesdex_t * cotesdex,
                   const char * fmt,
                   va_list ap)
    __attribute__((nonnull (1,2)));

void output_print(const cotesdex_t * cotesdex, const char * fmt, ...)
    __attribute__((format (printf, 2, 3), nonnull));

/*
 * Prints the buffer to the output commenting each line if the output is
 * currently a comment.
 * Returns the number of characters printed.
 */
int output_print_buffer(const cotesdex_t * cotesdex,
                        const char * buffer)
    __attribute__((nonnull));

void output_begin_comment(cotesdex_t * cotesdex)
    __attribute__((nonnull));
void output_end_comment(cotesdex_t * cotesdex)
    __attribute__((nonnull));

#endif /* !OUTPUT_H */
