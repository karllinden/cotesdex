/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* read_file.c - reads the content of a file and saves it in a buffer */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "io.h"
#include "nls.h"
#include "print-error.h"
#include "read-file.h"

char * __attribute__((nonnull))
read_file(const char * file_name, size_t * buffer_len)
{
    /* This is the buffer to fill in. */
    char * buffer = NULL;

    /* Open the file. */
    FILE * file = fopen(file_name, "r");
    if (file == NULL) {
        print_error_errno(_("could not open %s for reading"),
                file_name);
        goto error;
    }

    /* Find the file size. */
    if (fseek(file, 0L, SEEK_END)) {
        print_error_errno(_("could not seek %s"), file_name);
        goto error;
    }

    size_t size = (size_t)ftell(file);
    if (size == (size_t)(-1)) {
        print_error_errno(_("could not ftell %s"), file_name);
        goto error;
    }

    /* Seek to the beginning of the file again. */
    if (fseek(file, 0L, SEEK_SET)) {
        print_error_errno(_("could not seek %s"), file_name);
        goto error;
    }

    /*
     * Allocate the buffer. There is no +1 for the NUL-character because
     * the file ending (the line break at the end of the file) will be
     * replaced with the NUL character.
     *
     * xmalloc() is not used here because the opened file should not be
     * left open if the allocation fails.
     */
    buffer = malloc(size);
    if (buffer == NULL) {
        print_error_errno(_("could not allocate memory"));
        goto error;
    }

    /* Read everything into the buffer. */
    *buffer_len = fread(buffer, 1, size + 1, file);
    if (ferror(file)) {
        print_error(_("failed to read %s"), file_name);
        goto error;
    }

    /* The number of characters written should be equal to the size. */
    assert(*buffer_len == size);

    /* Make sure the end is reached. */
    assert(feof(file));

    /* The file must have a UNIX file ending. */
    assert(buffer[size-1] == '\n');

    /*
     * Replace the UNIX file ending (the line feed character) with the
     * NUL character so that the string is NUL terminated.
     */
    buffer[size-1] = '\0';

    if (false) {
    error:
        if (buffer != NULL) {
            free(buffer);
            buffer = NULL;
        }
    }

    if (file != NULL) {
        if (fclose(file)) {
            print_error_errno(_("could not close %s"), file_name);
        }
    }

    return buffer;
}
