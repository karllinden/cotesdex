/*
 * This file is part of cotesdex.
 *
 * Copyright (C) 2013-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* test.c - routines needed to generate a test script */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "buffer-foreach-line.h"
#include "command.h"
#include "file-type.h"
#include "main.h"
#include "nls.h"
#include "output.h"
#include "print-error.h"
#include "test.h"

#define oprint(...) output_print(cotesdex, __VA_ARGS__)
#define oputs(string) output_puts(cotesdex, (string))

/* A dummy struct to supress the const warning. */
struct content_s {
    const cotesdex_t * cotesdex;
};

struct print_command_line_s {
    const cotesdex_t * cotesdex;
    int had_line_before;
};

static char * __attribute__((nonnull))
get_executable(const cotesdex_t * cotesdex)
{
    const char * executable;
    char *       buffer;
    char *       c;

    if (cotesdex->executable_file_name != NULL) {
        buffer = strdup(cotesdex->executable_file_name);
        if (!buffer) {
            print_error_errno("could not allocate memory");
        }
        return buffer;
    }

    if (cotesdex->output_file_name != NULL) {
        executable = cotesdex->output_file_name;
    } else if (cotesdex->input_file_name != NULL) {
        executable = cotesdex->input_file_name;
    } else {
        print_error(_("could not guess the name of the executable "
                      "file to test"));
        print_error(_("please supply --executable=EXECUTABLE on "
                      "the command line"));
        return NULL;
    }

    buffer = strdup(executable);
    if (!buffer) {
        print_error_errno("could not allocate memory");
        return NULL;
    }

    /* Remove the suffix if there is one. */
    c = strrchr(buffer, '.');
    if (c != NULL) {
        *c = '\0';
    }

    return buffer;
}

static void __attribute__((nonnull))
content_line(const char * line, void * argument)
{
    struct content_s * cs = (struct content_s *)argument;
    output_print(cs->cotesdex, "%s\n", line);
    return;
}

static void __attribute__((nonnull(1,2,4)))
create_file(const cotesdex_t * cotesdex,
            const char * file,
            const char * content,
            const char * here_string)
{
    if (content[0] != '\0') {
        oprint("cat << '%s' > %s\n", here_string, file);
        struct content_s content_struct;
        content_struct.cotesdex = cotesdex;
        buffer_foreach_line(content, &content_line,
                            &content_struct);
        oprint("%s\n", here_string);
    } else {
        oprint("echo -n > %s\n", file);
    }
    return;
}

static void __attribute__((nonnull(1,2,4)))
create_file_and_variable(const cotesdex_t * cotesdex,
                         const char * file,
                         const char * content,
                         const char * here_string)
{
    /*
     * This is an unreadable, but hopefully efficient way of letting
     * assigning "\"${" + file + "}\"" to variable.
     */
    size_t file_len = strlen(file);
    char variable[file_len + 6];
    variable[0] = '\"';
    variable[1] = '$';
    variable[2] = '{';
    memcpy(variable + 3, file, file_len);
    variable[file_len + 3] = '}';
    variable[file_len + 4] = '\"';
    variable[file_len + 5] = '\0';

    if (content != NULL) {
        oprint("%s=\"${test_tmpdir}\"/%s\n", file, file);
        create_file(cotesdex, variable, content, here_string);
    } else {
        oprint("unset %s\n", file);
    }

    return;
}

static void __attribute__((nonnull))
print_command_line(const char * line, void * argument)
{
    struct print_command_line_s * pcs =
        (struct print_command_line_s *)argument;

    if (pcs->had_line_before) {
        output_putchar(pcs->cotesdex, ' ');
    }
    output_puts(pcs->cotesdex, line);
    pcs->had_line_before = 1;

    return;
}

static int __attribute__((nonnull))
create_expected_output_files(const cotesdex_t * cotesdex,
                             const command_t * command,
                             size_t file_name_len_max,
                             const char * here_string)
{
    int return_value = 1;

#define TEST_TMPDIR "\"${test_tmpdir}\"/"
#define TEST_TMPDIR_LEN 17
#define EXPECTED "_expected"
#define EXPECTED_LEN 9

    char expected_file[
        TEST_TMPDIR_LEN +
        file_name_len_max +
        EXPECTED_LEN +
        1
    ];
    memcpy(expected_file, TEST_TMPDIR, TEST_TMPDIR_LEN);

    /*
     * Loop through all commands to find any output file commands and
     * compare the results.
     */
    for (command_t * child = command_get_child(command);
         child != NULL;
         child = command_get_sister(child))
    {
        if (command_get_type(child) != COMMAND_OUTPUT_FILE) {
            continue;
        }

        const char * file_name = command_get_argument(child);
        char * content = command_file_harvest_content(child);
        if (content == NULL) {
            goto error;
        }

        /* Add the filename to the output_files. */
        oprint("output_files+=(\"%s\")\n", file_name);

        /*
         * Because the length of the strings are known memcpy is
         * used becaus it is somewhat faster. strcpy() has to check
         * for the '\0' character, while memcpy() does not.
         */
        size_t file_name_len = strlen(file_name);
        memcpy(expected_file + TEST_TMPDIR_LEN, file_name,
               file_name_len);
        memcpy(expected_file + TEST_TMPDIR_LEN + file_name_len,
               EXPECTED, EXPECTED_LEN);
        expected_file[TEST_TMPDIR_LEN + file_name_len + EXPECTED_LEN]
            = '\0';

        /* Create the file with the expected output. */
        create_file(cotesdex, expected_file, content, here_string);
        free(content);
    }

error:
    return return_value;
}

/*
 * Creates a test from the given test command.
 * Returns 0 on success and 1 on failure.
 */
static int __attribute__((nonnull))
create_test(const cotesdex_t * cotesdex,
            const command_t * command,
            const char * executable_name,
            int test_number,
            const char * here_string)
{
    int return_value = 1;

    struct print_command_line_s pcs;
    pcs.cotesdex = cotesdex;

    /* The maximum length of the names of the output files. */
    size_t output_file_name_len_max = 0;

    /* Assume INT_MIN is not a valid status. */
    int status = INT_MIN;

    /* Shall be free()'d. */
    char * command_line = NULL;
    char * stderr_content = NULL;
    char * stdin_content = NULL;
    char * stdout_content = NULL;

    command_t * child = command_get_child(command);
    while (child != NULL) {
        /* Variables needed for COMMAND_OUTPUT_FILE. */
        const char * output_file_name;
        size_t output_file_name_len;

        switch (command_get_type(child)) {
            case COMMAND_CMDLINE:
                command_line = command_harvest_content(child);
                child = command_unlink(child);
                break;
            case COMMAND_STATUS:
                status = atoi(command_get_argument(child));
                child = command_unlink(child);
                break;
            case COMMAND_STDERR:
                stderr_content = command_file_harvest_content(child);
                if (stderr_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;
            case COMMAND_STDIN:
                stdin_content = command_file_harvest_content(child);
                if (stdin_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;
            case COMMAND_STDOUT:
                stdout_content = command_file_harvest_content(child);
                if (stdout_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;

            /*
             * These cases do not unlink and destroy the command. The
             * command is to be used later on.
             */
            case COMMAND_OUTPUT_FILE:
                output_file_name = command_get_argument(child);
                output_file_name_len = strlen(output_file_name);
                if (output_file_name_len > output_file_name_len_max) {
                    output_file_name_len_max = output_file_name_len;
                }
            default:
                child = command_get_sister(child);
                break;
        }
    }

    oprint("test_number=%d\n", test_number);
    if (status != INT_MIN) {
        oprint("exit_status=%d\n", status);
    } else {
        oputs("unset exit_status\n");
    }
    oprint("test_tmpdir=\"$(mktemp --tmpdir -d \"%s-%d-XXXXXXXX\")\"\n",
           executable_name, test_number);

    create_file_and_variable(cotesdex, "stdin", stdin_content,
                             here_string);
    create_file_and_variable(cotesdex, "stdout_expected",
                             stdout_content, here_string);
    create_file_and_variable(cotesdex, "stderr_expected",
                             stderr_content, here_string);

    oputs("output_files=()\n");
    if (output_file_name_len_max > 0) {
        if (create_expected_output_files(cotesdex,
                                         command,
                                         output_file_name_len_max,
                                         here_string))
        {
            goto error;
        }
    }

    /* Run the tests with the given arguments. */
    oputs("run_test ");
    if (command_line != NULL) {
        pcs.had_line_before = 0;
        buffer_foreach_line(command_line, &print_command_line, &pcs);
    }
    oputs("\n"
          "run rmdir \"${test_tmpdir}\"\n\n");

error:
    return_value = 0;
    free(command_line);
    free(stderr_content);
    free(stdin_content);
    free(stdout_content);
    return return_value;
}

int __attribute__((nonnull))
test(cotesdex_t * cotesdex)
{
    int return_value = 1;

    /* To be free()'d if something fails. */
    char * executable = NULL;

    output_set_file_type(cotesdex, FILE_TYPE_SHELL);

    oprint("#!%s\n", BASH);

    output_begin_comment(cotesdex);
    oprint("Generated with %s\n", PACKAGE_STRING);

    char * title;
    char * description;
    char * license;
    get_meta(cotesdex, &title, &description, &license);

    /* Print title */
    if (title != NULL) {
        oprint("\n");
        oprint("Title: %s\n", title);
        free(title);
    }

    /* Print license */
    if (license != NULL) {
        oprint("\n");
        output_print_buffer(cotesdex, license);
        free(license);
    }

    /* Print description. */
    if (description != NULL) {
        oprint("\n");
        oprint("Description:\n");
        output_print_buffer(cotesdex, description);
        free(description);
    }

    output_end_comment(cotesdex);
    output_putchar(cotesdex, '\n');

    /* Set the executable in the output file. */
    executable = get_executable(cotesdex);
    if (!executable) {
        goto error;
    }
    oprint("executable=\"%s\"\n", executable);

    /*
     * Get the executable name that will be used in each test for
     * creating a temporary directory.
     */
    const char * executable_name = strrchr(executable, '/');
    if (executable_name) {
        executable_name++;
    } else {
        executable_name = executable;
    }

    oputs("script=$(basename \"${0}\")\n");
    if (cotesdex->memcheck) {
        oputs("memcheck=1\n");
    } else {
        oputs("unset memcheck\n");
    }
    oprint("if [ -z \"${COTESDEX_TEST_RUNNER}\" ]\n"
           "then\n"
           "    COTESDEX_TEST_RUNNER=\"%s/test-runner.sh\"\n"
           "fi\n"
           "source \"${COTESDEX_TEST_RUNNER}\" || exit 1\n",
           PKGDATADIR);

    /*
     * Change directory to the base path so that all input files can
     * be specified relative to the base path in the input file.
     */
    oprint("run cd \"%s\"\n\n", cotesdex->base_path);

    const char * here_string;
    if (cotesdex->here_string == NULL) {
        here_string = "__COTESDEX_HERE_STRING__";
    } else {
        here_string = cotesdex->here_string;
    }

    int test_number = 1;
    const command_t * root = cotesdex->commands;
    const command_t * command = command_get_child(root);
    while (command != NULL) {
        if (command_get_type(command) == COMMAND_TEST) {
            if (create_test(cotesdex, command, executable_name,
                            test_number++, here_string))
            {
                goto error;
            }
        }
        command = command_get_sister(command);
    }

    oprint("exit 0\n");

    if (cotesdex->output_file_name != NULL) {
        struct stat buf;
        if (stat(cotesdex->output_file_name, &buf)) {
            print_error_errno("could not stat %s",
                    cotesdex->output_file_name);
            goto error;
        }
        if (chmod(cotesdex->output_file_name, buf.st_mode | S_IXUSR)) {
            print_error_errno("could not chmod %s",
                    cotesdex->output_file_name);
            goto error;
        }
    }

    return_value = 0;
error:
    free(executable);
    return return_value;
}
