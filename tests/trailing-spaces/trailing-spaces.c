/*
 * @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex
 */

/* @cotesdex title Trailing spaces test */
/*
 * @cotesdex description
 * This example does nothing. Instead it tests that cotesdex does not
 * require trailing spaces to separate paragraphs. For example, it shall
 * not be required to write
 *
 * Paragraph 1
 * 
 * Paragraph 2
 *
 * where the paragraphs must be separated by a line with a trailing
 * space. Instead one should be able to separate paragraphs with empty
 * line containing no trailing spaces as is done in this example.
 * @cotesdex
 */

#include <stdio.h>

int
main(void)
{
    puts("Hello world!");
    return 0;
}
