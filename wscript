#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function

import os
import sys

import wafimport
from waflib import Build, Errors, Utils

VERSION = '1.0.0'
APPNAME = 'cotesdex'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

program_sources = [
    'src/asciidoc.c',
    'src/buffer-foreach-line.c',
    'src/command.c',
    'src/file-type.c',
    'src/main.c',
    'src/output.c',
    'src/parser.c',
    'src/print-error.c',
    'src/read-file.c',
    'src/strip.c',
    'src/test.c'
]

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')
    opt.load('autooptions')

    opt.set_auto_options_define('ENABLE_%s')

    opt.load('cotesdex')
    opt.load('nls')

    opt.add_auto_option(
            'static',
            'Build statically linked binaries',
            default=False)
    opt.add_auto_option(
            'time',
            'Build with time measuring support',
            default=False)

def configure(conf):
    conf.load('compiler_c')
    conf.load('gccdeps')
    conf.load('gnu_dirs')
    conf.load('werror')

    conf.env.append_unique('CFLAGS', '-std=gnu99')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.env['COTESDEX'] = True
    conf.env['NLS_FUNCTIONS'] = [
        'gettext',
        'bindtextdomain',
        'textdomain'
    ]
    conf.load('autooptions')
    conf.load('cotesdex')
    conf.load('nls')

    conf.env['UNLOCKED_FUNCTIONS'] = [
        'feof',
        'ferror',
        'fputc',
        'fread'
    ]
    conf.load('unlocked')

    pkg_config_args = [
        '--cflags',
        '--libs'
    ]
    if conf.env['STATIC']:
        pkg_config_args.append('--static')

    conf.check_cfg(
            package='gop-4',
            uselib_store='GOP',
            args=pkg_config_args)

    programs = [
        ('bash', '/bin/bash'),
        ('valgrind', '/usr/bin/valgrind')
    ]
    for prog,path in programs:
        # find_program_once is defined in the cotesdex wafimport.
        conf.find_program_once(prog, mandatory=False)
        name = prog.upper()
        if conf.env[name]:
            result = ' '.join(conf.env[name])
        elif name in os.environ:
            result = os.environ[name]
        else:
            result = path
        conf.define(name, result)

    conf.env['PACKAGE'] = conf.env['PACKAGE_NAME'] = APPNAME
    conf.env['PACKAGE_VERSION'] = conf.env['VERSION'] = VERSION
    conf.env['PACKAGE_URL'] = 'https://gitlab.com/karllinden/cotesdex'
    conf.env['PACKAGE_BUGREPORT'] = conf.env['PACKAGE_URL'] + '/issues'
    conf.env['PACKAGE_STRING'] = \
            conf.env['PACKAGE_NAME'] + ' ' + conf.env['VERSION']

    # nls
    conf.env['DOMAIN'] = conf.env['PACKAGE']
    conf.env['PACKAGE_DOMAIN'] = conf.env['DOMAIN']
    conf.env['POTFILES'] = program_sources
    conf.env['LINGUAS'] = [
            'sv'
    ]
    conf.env['COPYRIGHT_HOLDER'] = \
            Utils.readf('AUTHORS').splitlines()[0]
    conf.env['MSGID_BUGS_ADDRESS'] = conf.env['PACKAGE_BUGREPORT']

    conf.env['PKGCONFIGDIR'] = os.path.join(
            conf.env['LIBDIR'],
            'pkgconfig')
    conf.env['PKGDATADIR'] = os.path.join(
            conf.env['DATADIR'],
            conf.env['PACKAGE'])

    define_vars = [
        'LOCALEDIR',
        'PACKAGE',
        'PACKAGE_BUGREPORT',
        'PACKAGE_DOMAIN',
        'PACKAGE_NAME',
        'PACKAGE_STRING',
        'PACKAGE_URL',
        'PKGDATADIR',
        'VERSION'
    ]
    for x in define_vars:
        conf.define(x, conf.env[x])

    install_dirs = [
        ('PREFIX',      'Install prefix'),
        ('EXEC_PREFIX', 'Installation prefix for binaries'),
        ('BINDIR',      'Binary install dir'),
        ('LOCALEDIR',   'Locale data dir'),
        ('PKGDATADIR',  'Package data dir'),
    ]

    print()
    print('General configuration')
    conf.summarize_auto_options()
    print()
    print('Install directories')
    for name,descr in install_dirs:
        conf.msg(descr, conf.env[name], color='CYAN')

    conf.write_config_header('config.h')

def build(bld):
    # Load the werror wafimport explictly with the build function. The
    # reason for this is that this function is called from the check and
    # regenerate_expected functions.
    fun = bld.fun
    bld.fun = 'build'
    bld.load('werror')
    bld.fun = fun

    bld.env.append_unique('CFLAGS', '-DHAVE_CONFIG_H=1')
    bld.env['INCLUDES'] = [bld.bldnode.abspath()]

    linkflags = bld.env['STATIC'] and ['-static'] or []
    program = bld.bldnode.find_or_declare(bld.env['PACKAGE_NAME'])
    bld.program(
        target       = program,
        source       = program_sources,
        linkflags    = linkflags,
        use          = ['GOP', 'INTL'],
        install_path = bld.env['BINDIR']
    )

    bld(
        features     = 'subst',
        source       = 'cotesdex-valgrind.in',
        target       = 'cotesdex-valgrind',
        chmod        = Utils.O755,
        install_path = bld.env['BINDIR'],
        BASH         = bld.env['BASH'],
        BINDIR       = bld.env['BINDIR'],
        PACKAGE      = bld.env['PACKAGE'],
        VALGRIND     = bld.env['VALGRIND']
    )

    test_runner = bld.bldnode.find_or_declare('data/test-runner.sh')
    bld(
        features     = 'subst',
        source       = 'data/test-runner.sh.in',
        target       = test_runner,
        chmod        = Utils.O755,
        install_path = bld.env['PKGDATADIR'],
        BASH         = bld.env['BASH'],
        VALGRIND     = bld.env['VALGRIND']
    )

    # This is needed for the tests, since they must know where the
    # cotesdex binary is. Furthermore, define it here rather than in the
    # tests so that the environments for build and check are identical,
    # so that no node signatures are changed unnecessarily.
    bld.env['COTESDEX'] = program.abspath()
    bld.env['COTESDEX_TEST_RUNNER'] = test_runner.abspath()
    os.environ['COTESDEX_TEST_RUNNER'] = test_runner.abspath()

    bld.load('nls')

    return program

def recurse_into_tests(bld):
    # Call build so that ./waf check works even if the poor user forgets
    # to run .waf.
    build(bld)
    bld.recurse('tests')

check = recurse_into_tests
regenerate_expected = recurse_into_tests

class CheckClass(Build.BuildContext):
    cmd = 'check'
    fun = 'check'

class RegenerateExpectedClass(Build.BuildContext):
    cmd = 'regenerate-expected'
    fun = 'regenerate_expected'

def update_po(bld):
    bld.load('nls')
